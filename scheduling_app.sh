#!/bin/bash

########## ---------- MENU ---------- ##########
# Display Menu
menu() {
  echo -e "\nWELCOME TO SCHEDULING APP"
  echo "1. Add account"
  echo "2. Add schedule"
  echo "3. View schedule"
  echo "4. Suggest Schedule"
  echo -e "5. Quit\n"
}

# Schedule Menu
schedule_menu() {
  echo -e "\nSelect timeslot:"
  echo "1. 08:00 to 09:00"
  echo "2. 11:00 to 12:00"
  echo "3. 14:00 to 16:00"
  echo "4. Enter Time(e.g. hh:mm):"
}

# Suggest Schedule Loop
schedule_loop() {
  endtime=$(date "+%H:%M" -d "$time today + 1 hour")
  for i in "${name[@]}"; do
    endtime=$(date "+%H:%M" -d "$time today + 1 hour")
    if [[ -f $i.txt ]] ; then
      # Check if schedule already exists to selected attendee
      if grep -q "$date          Time: $time" $i.txt ; then
        echo "Schedule already plotted for $i."
      else
        echo "   Date: $date          Time: $time to $endtime" >> $i.txt
        echo "Schedule added to $i"
      fi
    else
      echo "NEW ACCOUNT DETECTED. Please add details for $i."
      add_account
      echo "   Date: $date          Time: $time to $endtime" >> $i.txt
      echo "Schedule added to $i"
    fi
  done
}

########## ---------- VALIDATE FUNCTIONS ---------- ##########
# Validate Email
emailformatcheck() {
  read -p "Email: " email
  # Check email format
  if [[ $email == *@*.* ]] ; then
    # Check if email already exists
    if  grep -q "$email" $name.txt ; then
      echo "EMAIL ADDRESS ALREADY EXISTS."
    else
      sed -i "1a Email: $email" $name.txt
    fi
  else
    echo "INVALID EMAIL FORMAT."
  fi
}

# Validate Date
datecheck() {
  read -p "Enter Date(e.g. yyyy-mm-dd): " date
  if [[ $(date "+%Y-%m-%d" -d $date) == $date ]] ; then
    read -p "Enter Time(e.g. hh:mm): " time
    timecheck
  else
    echo "INVALID DATE FORMAT."
  fi
}

# Validate Time
timecheck() {
  endtime=$(date "+%H:%M" -d "$time today + 1 hour")
  if [[ $(date "+%H:%M" -d $time) == $time ]] ; then
    for ((i=0 ; i<$attendees ; i++)); do
      read -p "Attendee: " name
      if [[ -f $name.txt ]] ; then
        # Check if schedule already exists to selected attendee
        if grep -q "$date          Time: $time" $name.txt ; then
          echo "Schedule already plotted for $name."
        else
          echo "   Date: $date          Time: $time to $endtime" >> $name.txt
          echo "Schedule added to $name"
        fi
      else
        echo "NEW ACCOUNT DETECTED. Please add details for $name."
        add_account
        echo "   Date: $date          Time: $time to $endtime" >> $name.txt
        echo "Schedule added to $name"
      fi
    done
  else
    echo "INVALID TIME FORMAT."
  fi
}

########## ---------- OPTION FUNCTIONS ---------- ##########
# Add Account
add_account() {
    echo "Enter User Details"
    read -p "Name: " name
    if [[ -f $name.txt ]] ; then
      echo "NAME ALREADY EXISTS."
      emailformatcheck
    else
      echo "Schedule of $name:" > $name.txt
      emailformatcheck
    fi
}

# Add Schedule
add_schedule() {
  read -p "Enter Number of Attendees: " attendees
  if [[ $attendees =~ ^[0-9]+$ ]]; then
    datecheck
  else
    echo -e "PLEASE ENTER NUMBER OF ATTENDEES.\n"
  fi
}

# View Schedule
view_schedule() {
  read -p "Enter Name: " name
  if [[ -f $name.txt ]] ; then
    cat $name.txt
  else
    echo "No Existing Schedule for $name"
  fi
}

# Suggest Schedule
suggest_schedule() {
  echo "Enter name of Attendees: (e.g. name1 name2 name3 name4)"
  read -a name
  schedule_menu
  read -p "Enter timeslot: " suggestion_choice
    case $suggestion_choice in
      1)
        read -p "Enter Date(e.g. yyyy-mm-dd): " date
        if [[ $(date "+%Y-%m-%d" -d $date) == $date ]] ; then
          suggestion="08:00"
          time=$suggestion
          schedule_loop
        else
          echo "INVALID DATE FORMAT."
        fi
      ;;
      2)
        read -p "Enter Date(e.g. yyyy-mm-dd): " date
        if [[ $(date "+%Y-%m-%d" -d $date) == $date ]] ; then
          suggestion="11:00"
          time=$suggestion
          schedule_loop
        else
          echo "INVALID DATE FORMAT."
        fi
      ;;
      3)
        read -p "Enter Date(e.g. yyyy-mm-dd): " date
        if [[ $(date "+%Y-%m-%d" -d $date) == $date ]] ; then
          suggestion="14:00"
          time=$suggestion
          schedule_loop
        else
          echo "INVALID DATE FORMAT."
        fi
      ;;
      4) 
        read -p "Enter Time(e.g. hh:mm):" suggestion
        read -p "Enter Date(e.g. yyyy-mm-dd): " date
        if [[ $(date "+%Y-%m-%d" -d $date) == $date ]] ; then
          time=$suggestion
          schedule_loop
        else
          echo "INVALID DATE FORMAT."
        fi
      ;;
    esac
}

########## ---------- WELCOME FUNCTIONS ---------- ##########
# Welcome Display
welcome() {
  while true; do
    menu
    read -r option
    case $option in
      1) add_account;;
      2) add_schedule;;
      3) view_schedule;;
      4) suggest_schedule;;
      5)
        echo "Thank you for using Scheduling App";
        break
      ;;
      *) echo -e "INVALID OPTION. PLEASE CHOOSE AGAIN.\n";;
    esac
  done
}

welcome